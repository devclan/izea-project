import Component from '@ember/component';

export default Component.extend({
  actions: {
    closeSidebar(){
      this.element.classList.remove('posts-index__sidebar--open');
    }
  },

  didReceiveAttrs(){
    this._super(...arguments);
    if (this.element) {
      if (this.selectedPost) {
        this.element.classList.add('posts-index__sidebar--open');
      } else {
        this.element.classList.remove('posts-index__sidebar--open');
      }
    }
  }
});
