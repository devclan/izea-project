import Component from '@ember/component';

export default Component.extend({
  actions: {
    clickTitleOf(post){
      this.setSelected(post);
    }
  },

  init(){
    this._super(...arguments);
    this.set('isCurrentPost', false);
  },

  didReceiveAttrs(){
    this._super(...arguments);
    if (this.element) {
      if (this.selectedPost.id == this.post.id) {
        this.set('isCurrentPost', true);
      } else {
        this.set('isCurrentPost', false);
      }
    }
  }
});
