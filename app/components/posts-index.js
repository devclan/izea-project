import Component from '@ember/component';

export default Component.extend({
  sassOptions: {
    includePaths: ['components/posts-index']
  },

  ajax: Ember.inject.service(),
  actions: {
    setSelected(post){
      this._setSelected(post);
    }
  },

  notTheSameAuthorOfThe(post) {
    if (this.selectedPost && post.userId === this.selectedPost.author.id) {
      return false
    } else {
      return true
    }
  },

  _setSelected(post){
    if (!post) {
      return false;
    }

    if (this.notTheSameAuthorOfThe(post)) {
      let promise = this.get('ajax').request(`${this.usersPath}/${post.userId}`);

      promise.then((author) => {
        let selectedPost = Object.assign({}, post, { author: author });
        this.set('selectedPost', selectedPost);
      })

      return promise;

    } else {

      this.set('selectedPost', Object.assign({}, this.selectedPost, post));
    }
  }
});
