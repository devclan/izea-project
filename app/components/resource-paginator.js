import Component from '@ember/component';

export default Component.extend({
  setLastPage(){
    this.lastPage = Math.ceil(this.resourceCount / this.resourcesPerPage);
  },

  setPreviousPage(){
    let previousPage = null;

    if (this.currentPage > 1) {
      previousPage = this.currentPage - 1;
    }

    this.set('previousPage', previousPage);
  },

  setNextPage(){
    let nextPage = null;

    if (this.currentPage < this.lastPage) {
      nextPage = this.currentPage + 1;
    }

    this.set('nextPage', nextPage);
  },

  setInRange(){
    let inRange = (this.currentPage >= 1 && this.currentPage <= this.lastPage);
    this.set('inRange', inRange);
  },

  goToCurrentPage(){
    this.setPreviousPage();
    this.setNextPage();
    this.setInRange();
  },

  init(){
    this._super(...arguments);
    this.setLastPage();
    this.goToCurrentPage();
  },

  didUpdateAttrs(){
    this._super(...arguments);
    this.goToCurrentPage();
  }
});
