import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  ajax: Ember.inject.service(),
  postsPath: 'https://jsonplaceholder.typicode.com/posts',
  usersPath: 'https://jsonplaceholder.typicode.com/users',
  postsPerPage: 15,

  queryParams: {
    page: { refreshModel: true }
  },

  startAndEndForPage(page) {
    let start = (page - 1) * this.postsPerPage;
    let end = start + this.postsPerPage;
    return [start, end];
  },

  getPosts(_start, _end){
    return this.get('ajax').request(this.postsPath, {
      data: {
        _start: _start,
        _end: _end
      }
    })
  },

  model(params){
    let [_start, _end] = this.startAndEndForPage(params.page);
    return RSVP.hash({
      posts: this.getPosts(_start, _end),
      postsPath: this.postsPath,
      usersPath: this.usersPath,
      currentPage: params.page,
      postsPerPage: this.postsPerPage
    });
  },
});
